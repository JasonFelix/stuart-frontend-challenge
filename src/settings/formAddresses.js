import dropOffBadgeBlank from '../assets/images/dropOffBadgeBlank.svg';
import dropOffBadgeError from '../assets/images/dropOffBadgeError.svg';
import dropOffBadgePresent from '../assets/images/dropOffBadgePresent.svg';
import dropOffMarker from '../assets/images/dropOffMarker.svg';

import pickUpBadgeBlank from '../assets/images/pickUpBadgeBlank.svg';
import pickUpBadgeError from '../assets/images/pickUpBadgeError.svg';
import pickUpBadgePresent from '../assets/images/pickUpBadgePresent.svg';
import pickUpMarker from '../assets/images/pickUpMarker.svg';

export default [
  {
    name: 'pickUp',
    placeholder: 'Pick up address',
    images: {
      blank: pickUpBadgeBlank,
      error: pickUpBadgeError,
      present: pickUpBadgePresent,
      marker: pickUpMarker,
    },
  },
  {
    name: 'dropOff',
    placeholder: 'Drop off address',
    images: {
      blank: dropOffBadgeBlank,
      error: dropOffBadgeError,
      present: dropOffBadgePresent,
      marker: dropOffMarker,
    },
  },
];
