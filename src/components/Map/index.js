/* eslint-disable no-undef */

import React from 'react';
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  OverlayView,
} from 'react-google-maps';

const Map = withScriptjs(withGoogleMap(props => (
  <GoogleMap
    options={{ mapTypeControl: false, fullscreenControl: false }}
    defaultZoom={15}
    defaultCenter={{ lat: 48.869827, lng: 2.334579 }}
  >
    {
        Object.keys(props.addresses).map((key) => {
          const address = props.addresses[key];
          if (!address.latlng) {
            return <div key={`${address.name} Math.random()`} />;
          }
          return (
            <OverlayView
              key={`${address.name} Math.random()`}
              mapPaneName={OverlayView.OVERLAY_LAYER}
              position={{ lat: address.latlng.lat, lng: address.latlng.lng }}
              getPixelPositionOffset={(w, h) => ({ x: -(w / 2), y: -(h) })}
            >
              <img src={address.marker} alt="dropoff" />
            </OverlayView>
          );
        })
      }
  </GoogleMap>
)));


export default props =>
  (<Map
    {...props}
    googleMapURL="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places"
    loadingElement={<div style={{ height: '100%' }} />}
    containerElement={<div style={{ flex: 1 }} />}
    mapElement={<div style={{ height: '100%' }} />}
  />);

