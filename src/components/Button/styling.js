/* eslint-disable */

import styled from 'styled-components';

export const Container = styled.div.attrs({
  opacity: props => (props.enabled ? '1' : '.5'),
  cursor: props => (props.enabled ? 'pointer' : 'not-allowed;'),
})`
  font-family: 'Roboto Bold';
  opacity: ${props => props.opacity};
  background: linear-gradient(0deg, #0f99e8, #10a2ea);
  box-shadow: 0 1px 2px 0 rgba(16, 162, 234, .30);
  width: 325px;
  display: flex;
  flex: 1;
  justify-content: center;
  color: white;
  align-items: center;
  border-radius: 4px;
  height: 40px;
  cursor: ${props => props.cursor};
`;
