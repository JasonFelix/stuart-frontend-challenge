import React from 'react';
import PropTypes from 'prop-types';
import { Container } from './styling';

const Button = props => <Container {...props}>{props.name}</Container>;

Button.propTypes = {
  name: PropTypes.string.isRequired,
};

export default Button;

