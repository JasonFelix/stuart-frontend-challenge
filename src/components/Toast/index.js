import React from 'react';
import PropTypes from 'prop-types';
import { Container, ToastContainer } from './styling';

const Toast = props =>
  (
    <Container {...props}>
      <ToastContainer {...props}>{props.title}</ToastContainer>
    </Container>
  );

Toast.propTypes = {
  title: PropTypes.string.isRequired,
};

export default Toast;
