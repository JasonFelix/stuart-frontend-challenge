import styled from 'styled-components';

export const Container = styled.div.attrs({
  right: props => (props.visible ? '32px' : '0'),
})`
  position: absolute;
  top: 32px;
  right: ${props => props.right};
  z-index: 1000;
  transition: 1s;  
`;

export const ToastContainer = styled.div.attrs({
  opacity: props => (props.visible ? '1' : '0'),
})`
  border-radius: 8px;
  height: 30px;
  background: rgba(51, 51, 51, 0.9);
  opacity: ${props => props.opacity};
  color: white;
  transition: 0.5s;
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.1), 0 1px 8px 0 rgba(0, 0, 0, 0.1);
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 5px 20px;
  cursor: pointer;
`;
