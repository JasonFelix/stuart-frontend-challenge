import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Container, ImageContainer, TextInputContainer, Input } from './styling';

const ON_TYPE_DELAY = 500; // 0.5 seconds

function handleNetworkError(res) {
  return res.ok ? res : res.json().then((err) => { throw err; });
}

class Address extends Component {
  constructor(props) {
    super(props);
    this.inputTimer = null;
    this.state = { upToDate: false, isValid: false, lastGeocode: '' };

    this.onChange = e => this.fetchAddress(e, ON_TYPE_DELAY);
    this.onBlur = e => this.fetchAddress(e, 0);
  }

  componentWillUnmount() {
    clearTimeout(this.inputTimer);
  }

  handleGeocoding(json) {
    if (json.address) {
      const { latitude, longitude } = json;
      this.props.onValidityChange(true, { lat: latitude, lng: longitude });
      this.setState({ upToDate: true, isValid: true, lastGeocode: '' });
    } else {
      this.props.onValidityChange(false);
      this.setState({ upToDate: true, isValid: false, lastGeocode: '' });
    }
  }

  fetchAddress(e, timeout) {
    const address = e.target.value;
    if (address === this.state.lastGeocode) return;

    clearTimeout(this.inputTimer);
    this.props.onValidityChange(false);
    this.props.onTextChanged(address);
    this.setState({ upToDate: false, isValid: false, lastGeocode: address });

    this.inputTimer = setTimeout(
      () =>
        fetch('http://localhost:4000/geocode', {
          method: 'post',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({ address }),
        })
          .then(handleNetworkError)
          .then(res => res.json())
          .then(json => this.handleGeocoding(json))
          .catch(error => console.info(`${error.code}: ${error.message}`))
      , timeout,
    );
  }

  render() {
    return (
      <Container>
        {
          this.props.image &&
            <ImageContainer
              upToDate={this.state.upToDate}
              isValid={this.state.isValid}
              src={this.props.image(this.state.upToDate, this.state.isValid)}
              validColor="#FDB036"
              invalidColor="#FC4960"
            />
        }
        <TextInputContainer>
          <Input onBlur={this.onBlur} onChange={this.onChange} {...this.props} type="text" />
        </TextInputContainer>
      </Container>
    );
  }
}

Address.defaultProps = {
  image: () => {},
};

Address.propTypes = {
  image: PropTypes.func,
  onValidityChange: PropTypes.func.isRequired,
  onTextChanged: PropTypes.func.isRequired,
};

export default Address;
