import React from 'react';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import { Container } from './styling';

configure({ adapter: new Adapter() });

describe('<Container />', () => {
  it('should render a <div> /', () => {
    const renderedComponent = shallow(<Container />);
    expect(renderedComponent.type()).toEqual('div');
  });
});
