import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex: 1;
  flex-direction: row;
  align-items: center;
`;

export const ImageContainer = styled.img`
  width: 32px;
  height: 32px;
  border-radius: 100%;
  margin-right: 8px;
`;

export const TextInputContainer = styled.div`
  width: 305px;
  display: flex;
  flex: 1;
  border-radius: 4px;
  height: 32px;
  background: #f0f3f7;
  padding: 0 10px;
`;

export const Input = styled.input`
  ::placeholder {
    color: #8596a6;
  }
  flex: 1;
  font-size: 16px;
  color: #252525;
  outline: none;
  background: none;
  border: none;
`;
