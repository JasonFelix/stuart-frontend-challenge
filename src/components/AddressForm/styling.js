import styled from 'styled-components';

export const Container = styled.div`
  margin: 32px;
  position: absolute;
  padding: 16px;
  background: white;
  border-radius: 8px;
  box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.1), 0 1px 8px 0 rgba(0, 0, 0, 0.1);
  display: flex;
  flex: 1;
  flex-direction: column;
  align-items: flex-end;
`;

export const Spacing = styled.div`
  margin-bottom: 16px;
`;
