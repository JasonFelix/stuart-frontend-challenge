import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { Container, Spacing } from './styling';
import AddressInput from '../AddressInput';
import Button from '../Button';

class AddressForm extends Component {
  static addressImage(location, images) {
    return (upToDate, isValid) => {
      if (!upToDate || location === '') {
        return images.blank;
      } else if (isValid) {
        return images.present;
      }
      return images.error;
    };
  }

  constructor(props) {
    super(props);
    const defaultState = {};
    this.props.addresses.forEach((address) => {
      defaultState[address.name] = {
        name: address.name,
        valid: false,
        location: '',
      };
    });
    this.state = defaultState;

    this.onAddressChange = address => location => this.onAddressChanged(address, location);
    this.onValidityChange = address =>
      (valid, location) => this.onValidityChanged(address, valid, location);
  }

  onAddressChanged(address, location) {
    this.setState({
      ...this.state,
      [address.name]: {
        name: address.name,
        valid: false,
        location,
      },
    });
  }

  onValidityChanged(address, valid, location = {}) {
    const { lat, lng } = location;
    this.setState({
      ...this.state,
      [address.name]: {
        ...this.state[address.name],
        marker: address.images.marker,
        valid,
        latlng: lat && lng ? { lat, lng } : null,
      },
    }, () => this.props.onLocationUpdate({ updatedAddress: address.name, addresses: this.state }));
  }

  onClick() {
    if (this.isButtonEnabled()) {
      this.props.button.onClick(this.state, () => this.resetForm());
    }
  }

  resetForm() {
    const blankState = {};
    this.props.addresses.forEach((address) => {
      blankState[address.name] = {
        name: address.name,
        valid: false,
        location: '',
      };
    });
    this.setState(blankState);
  }

  isButtonEnabled() {
    return !_.find(this.state, address => !address.valid);
  }

  render() {
    return (
      <Container>
        {
          this.props.addresses.map(address =>
            (
              <Spacing key={address.name}>
                <AddressInput
                  value={this.state[address.name].location || ''}
                  onTextChanged={this.onAddressChange(address)}
                  onValidityChange={this.onValidityChange(address)}
                  image={
                    AddressForm.addressImage(this.state[address.name].location, address.images)
                  }
                  placeholder={address.placeholder}
                />
              </Spacing>
            ))
        }
        <Button
          onClick={() => this.onClick()}
          enabled={this.props.button.enabled && this.isButtonEnabled()}
          name={this.props.button.name}
        />
      </Container>
    );
  }
}

AddressForm.propTypes = {
  button: PropTypes.shape({
    name: PropTypes.string.isRequired,
    enabled: PropTypes.bool.isRequired,
    onClick: PropTypes.func.isRequired,
  }).isRequired,
  addresses: PropTypes.arrayOf(PropTypes.object).isRequired,
  onLocationUpdate: PropTypes.func.isRequired,
};

export default AddressForm;
