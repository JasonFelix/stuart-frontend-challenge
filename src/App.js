import React, { Component } from 'react';
import { Container } from './styled';
import AddressForm from './components/AddressForm';
import formAddresses from './settings/formAddresses';
import Map from './components/Map';
import Toast from './components/Toast';

const TOAST_TIMER = 5000; // Five seconds

class App extends Component {
  constructor(props) {
    super(props);
    this.timer = null;
    this.submitJobForm = addresses => this.submitForm(addresses);
    this.state = { creatingJob: false, addresses: {}, toast: false };
  }

  componentWillUnmount() {
    clearTimeout(this.timer);
  }

  onJobCreated() {
    this.popToast();
    this.setState({ addresses: {}, creatingJob: false });
  }

  async submitForm(addresses) {
    this.setState({ creatingJob: true });

    const { pickUp, dropOff } = addresses;
    const body = JSON.stringify({ pickup: pickUp.location, dropoff: dropOff.location });
    const headers = { Accept: 'application/json', 'Content-Type': 'application/json' };
    return fetch('http://localhost:4000/jobs', {
      method: 'post',
      headers,
      body,
    })
      .then(response => response.json())
      .then(this.onJobCreated())
      .catch(() => this.setState({ addresses: {}, creatingJob: false }));
  }

  popToast() {
    this.setState({ toast: true }, () => {
      clearTimeout(this.timer);
      this.timer = setTimeout(() => this.setState({ toast: false }), TOAST_TIMER);
    });
  }

  render() {
    return (
      <Container>
        <Map addresses={this.state.addresses} />
        <AddressForm
          onLocationUpdate={locations => this.setState({ addresses: locations.addresses })}
          button={{
            name: this.state.creatingJob ? 'Creating...' : 'Create job',
            enabled: !this.state.creatingJob,
            onClick: (addresses, resetForm) =>
              this.submitJobForm(addresses).then(() => resetForm()),
          }}
          addresses={formAddresses}
        />
        <Toast
          onClick={() => this.setState({ toast: false })}
          visible={this.state.toast}
          title="Job has been created successfully!"
        />
      </Container>
    );
  }
}

export default App;
