# Jason Felix's submission for Stuart's frontend Challenge

![banner](https://imgur.com/zfn3yhD.png)
#### Set up & run
```
$ yarn install
$ yarn start
```
#### Design choices

##### Not using Redux
I decided against using redux as it would not provide any improvments to the codes readability or flexibility.
##### Decoupling the AddressForm and the Map
I kept the Map and the AddressForm seperate to allow the AddressForm component to be used without the map.
##### Using a config to set the AddressInput in the AddressForm
This was done to make setting up a the AddressForm component with any number of addresses as easy as possible.
##### No "Invalid (Pick up or Dropoff) Address" Message
The reason I didn't implement this in the input field was because it could cause problems for slow typers.

#### Improvements
* Fit bounds when a valid location is entered
* Network errors should be shown in the interface - perhaps with the Toast
* Instead of using mutiple svgs with different background colors, use a transparent background and colour using css.
* When the toast is clicked it could go to the job show page 
* A button on the interface that allows the user to add for address fields
* Given more time I would create a standalone AddressForm module.
